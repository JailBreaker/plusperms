package eu.jailbreaker.permissions;

import eu.jailbreaker.permissions.commands.PermissionsCommand;
import eu.jailbreaker.permissions.listeners.PermissionJoinQuitListener;
import eu.jailbreaker.permissions.utils.Group;
import eu.jailbreaker.permissions.utils.PermissionMessages;
import eu.jailbreaker.permissions.utils.PermissionPlayer;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/********************************************************************************
 *    Urheberrechtshinweis                                                      *
 *    Copyright © JailBreaker 2018                                           *
 *    Projekt: Permissions                                                  *
 *    Erstellt: 15.12.2018 / 16:27                                               *
 *                                                                              *
 *    Alle Inhalte dieses Quelltextes sind urheberrechtlich geschützt.          *
 *    Das Urheberrecht liegt, soweit nicht ausdrücklich anders gekennzeichnet,  *
 *    bei JailBreaker. Alle Rechte vorbehalten.                                 *
 *                                                                              *
 *    Jede Art der Vervielfältigung, Verbreitung, Vermietung, Verleihung,       *
 *    öffentlichen Zugänglichmachung oder andere Nutzung                        *
 *    bedarf der ausdrücklichen, schriftlichen Zustimmung von JailBreaker.      *
 *                                                                              *
 *    YouTube: https://www.youtube.com/JailBreaker                              *
 *    Twitter: https://twitter.com/JailBreakerTV                                *
 *    Webseite: https://www.jailbreaker.eu                                      *
 *    Teamspeak: ts.jailbreaker.eu                                              *
 *******************************************************************************/

@Getter
@Setter
public class PermissionsHandler extends JavaPlugin {

    private static PermissionsHandler plugin;
    private final Map<String, PermissionPlayer> players = new HashMap<>();
    private final Map<String, Group> groups = new HashMap<>();
    private PermissionMessages messages;
    private File file;
    private Group defaultGroup;

    @Override
    public void onEnable() {
        plugin = this;
        getCommand("permissions").setExecutor(new PermissionsCommand(this));
        getServer().getPluginManager().registerEvents(new PermissionJoinQuitListener(this), this);

        load();
    }

    public void load() {
        saveDefaultConfig();
        messages = new PermissionMessages(this);

        final YamlConfiguration config = (YamlConfiguration) getConfig();

        for (String groupName : config.getConfigurationSection("Groups").getKeys(false)) {
            Group group = new Group(groupName, config.getStringList("Groups." + groupName + ".Permissions"));
            group.setDisplay(config.getString("Groups." + groupName + ".Display").replaceAll("&", "§"));
            group.setPrefix(config.getString("Groups." + groupName + ".Prefix").replaceAll("&", "§"));
            group.setSuffix(config.getString("Groups." + groupName + ".Suffix").replaceAll("&", "§"));
            groups.put(groupName, group);
        }

        for (String playerName : config.getConfigurationSection("Database").getKeys(false)) {
            Group group = groups.get(config.getString("Database." + playerName + ".Group"));
            PermissionPlayer permissionPlayer = new PermissionPlayer(playerName, group);
            players.put(playerName, permissionPlayer);
        }

        defaultGroup = new Group(config.getString("Default"), config.getStringList("Groups." + config.getString("Default") + ".Permissions"));
        defaultGroup.setDisplay(config.getString("Groups." + config.getString("Default") + ".Display").replaceAll("&", "§"));
        defaultGroup.setPrefix(config.getString("Groups." + config.getString("Default") + ".Prefix").replaceAll("&", "§"));
        defaultGroup.setSuffix(config.getString("Groups." + config.getString("Default") + ".Suffix").replaceAll("&", "§"));
    }

    public void addGroupPermission(final String permission, final String groupName) {
        final Group group = groups.get(groupName);

        group.getPermissions().add(permission);
        getConfig().set("Groups." + groupName + ".Permissions", group.getPermissions());
        groups.replace(groupName, group);
        saveConfig();
    }

    public void removeGroupPermission(final String permission, final String groupName) {
        final Group group = groups.get(groupName);

        group.getPermissions().remove(permission);
        getConfig().set("Groups." + groupName + ".Permissions", group.getPermissions());
        groups.replace(groupName, group);
        saveConfig();
    }

    public boolean groupExists(final String groupname) {
        return groups.containsKey(groupname);
    }

    public void createPlayer(final String player) {
        if (!players.containsKey(player)) {
            final PermissionPlayer permissionPlayer = new PermissionPlayer(player, defaultGroup);
            getConfig().set("Database." + player + ".Group", permissionPlayer.getGroup().getName());
            saveConfig();
        }
    }

    public void setGroup(final Group group, final String player) {
        final PermissionPlayer permissionPlayer = players.get(player);
        permissionPlayer.setGroup(group);

        if (getServer().getPlayer(player) != null)
            getServer().getPlayer(player).kickPlayer("New Rank");

        getConfig().set("Database." + player + ".Group", group.getName());
        saveConfig();
    }

}

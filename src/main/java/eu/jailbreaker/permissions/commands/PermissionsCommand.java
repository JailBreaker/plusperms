package eu.jailbreaker.permissions.commands;

import com.google.common.base.Joiner;
import eu.jailbreaker.permissions.PermissionsHandler;
import eu.jailbreaker.permissions.utils.Group;
import eu.jailbreaker.permissions.utils.PermissionPlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;

/********************************************************************************
 *    Urheberrechtshinweis                                                      *
 *    Copyright © JailBreaker 2018                                           *
 *    Projekt: Permissions                                                  *
 *    Erstellt: 15.12.2018 / 16:27                                               *
 *                                                                              *
 *    Alle Inhalte dieses Quelltextes sind urheberrechtlich geschützt.          *
 *    Das Urheberrecht liegt, soweit nicht ausdrücklich anders gekennzeichnet,  *
 *    bei JailBreaker. Alle Rechte vorbehalten.                                 *
 *                                                                              *
 *    Jede Art der Vervielfältigung, Verbreitung, Vermietung, Verleihung,       *
 *    öffentlichen Zugänglichmachung oder andere Nutzung                        *
 *    bedarf der ausdrücklichen, schriftlichen Zustimmung von JailBreaker.      *
 *                                                                              *
 *    YouTube: https://www.youtube.com/JailBreaker                              *
 *    Twitter: https://twitter.com/JailBreakerTV                                *
 *    Webseite: https://www.jailbreaker.eu                                      *
 *    Teamspeak: ts.jailbreaker.eu                                              *
 *******************************************************************************/

public class PermissionsCommand implements CommandExecutor {

    private final PermissionsHandler plugin;

    public PermissionsCommand(final PermissionsHandler plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

        if (!(sender instanceof ConsoleCommandSender)) {
            if (!(sender.hasPermission("plusperms.use"))) {
                sender.sendMessage(plugin.getMessages().getNoPerm());
                return false;
            }
        }

        if (args.length == 0) {
            help(sender);
            return false;
        }

        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("groups")) {
                sender.sendMessage(plugin.getMessages().getGroupListTitle());
                sender.sendMessage(plugin.getMessages().getPrefix() + "§7Gruppen§8: " + plugin.getMessages().getMainColor() + Joiner.on("§7, " + plugin.getMessages().getMainColor()).join(plugin.getGroups().keySet()));
            } else if (args[0].equalsIgnoreCase("check")) {
                if (sender.hasPermission("de.pluginstube.test")) {
                    sender.sendMessage("§aDu hast die Rechte");
                } else {
                    sender.sendMessage("§cDu hast die Rechte nicht");
                }
            } else if (args[0].equalsIgnoreCase("reload")) {
                plugin.load();
            } else {
                help(sender);
            }
        } else if (args.length == 2) {
            if (args[0].equalsIgnoreCase("createGroup")) {
                if (plugin.groupExists(args[1])) {
                    sender.sendMessage(plugin.getMessages().getGroupExists());
                    return false;
                }

                plugin.getConfig().set("Groups." + args[1] + ".Display", "&e");
                plugin.getConfig().set("Groups." + args[1] + ".Prefix", "&e" + args[1] + " &8:&4 ");
                plugin.getConfig().set("Groups." + args[1] + ".Suffix", "&e");
                plugin.getConfig().set("Groups." + args[1] + ".Permissions", plugin.getDefaultGroup().getPermissions());

                plugin.saveConfig();

                Group group = new Group(args[1], plugin.getDefaultGroup().getPermissions());
                plugin.getGroups().put(args[1], group);

                sender.sendMessage(plugin.getMessages().getMessage(plugin.getMessages().getGroupCreated(), args[1]));
            } else if (args[0].equalsIgnoreCase("deleteGroup")) {

                if (!plugin.groupExists(args[1])) {
                    sender.sendMessage(plugin.getMessages().getGroupDoesNotExists());
                    return false;
                }

                plugin.getConfig().set("Groups." + args[1], null);

                for (PermissionPlayer permissionPlayer : plugin.getPlayers().values()) {
                    if (permissionPlayer.getGroup().getName().equalsIgnoreCase(args[1])) {
                        plugin.setGroup(plugin.getDefaultGroup(), permissionPlayer.getName());
                    }
                }
                plugin.saveConfig();
                plugin.getGroups().remove(args[1]);
                sender.sendMessage(plugin.getMessages().getMessage(plugin.getMessages().getGroupDeleted(), args[1]));

            } else if (args[0].equalsIgnoreCase("getGroup")) {
                final Group group = plugin.getGroups().get(args[1]);
                if (group == null) {
                    sender.sendMessage(plugin.getMessages().getGroupDoesNotExists());
                    return false;
                }
                sender.sendMessage(plugin.getMessages().getMessage(plugin.getMessages().getGroupInformation(), (group.getDisplay() + group.getName()), (group.getPrefix() + "§o" + sender.getName()), (group.getDisplay() + "§oFarbe"), (group.getSuffix() + "§oFarbe"), (Joiner.on("§7, §e").join(group.getPermissions()))));
            } else if (args[0].equalsIgnoreCase("getPlayer")) {
                final PermissionPlayer permissionPlayer = plugin.getPlayers().get(args[1]);
                if (permissionPlayer == null) {
                    sender.sendMessage(plugin.getMessages().getPlayerDoesNotExists());
                    return false;
                }

                sender.sendMessage(plugin.getMessages().getMessage(plugin.getMessages().getPlayerInformation(), permissionPlayer.getName(), permissionPlayer.getGroup().getName()));
            } else {
                help(sender);
            }
        } else if (args.length == 3) {
            final String arg2 = args[1], arg3 = args[2];

            switch (args[0].toLowerCase()) {
                case "add":

                    if (!plugin.groupExists(arg2)) {
                        sender.sendMessage(plugin.getMessages().getGroupDoesNotExists());
                        return false;
                    }
                    if (plugin.getGroups().get(arg2).hasPermission(arg3)) {
                        sender.sendMessage(plugin.getMessages().getGroupAlreadyHasPermission());
                        return false;
                    }

                    plugin.addGroupPermission(arg3, arg2);
                    sender.sendMessage(plugin.getMessages().getMessage(plugin.getMessages().getPermissionAdded(), arg2, arg3));
                    break;
                case "remove":
                    if (!plugin.groupExists(arg2)) {
                        sender.sendMessage(plugin.getMessages().getGroupDoesNotExists());
                        return false;
                    }
                    if (!plugin.getGroups().get(arg2).hasPermission(arg3)) {
                        sender.sendMessage(plugin.getMessages().getGroupDoesNotHavePermission());
                        return false;
                    }
                    plugin.removeGroupPermission(arg3, arg2);


                    for (PermissionPlayer permissionPlayer : plugin.getPlayers().values()) {
                        if (permissionPlayer.getGroup().getName().equalsIgnoreCase(arg2)) {
                            plugin.setGroup(plugin.getGroups().get(permissionPlayer.getGroup().getName()), permissionPlayer.getName());
                        }
                    }

                    sender.sendMessage(plugin.getMessages().getMessage(plugin.getMessages().getPermissionRemoved(), arg2, arg3));
                    break;
                case "setgroup":

                    plugin.createPlayer(arg2);

                    if (!plugin.groupExists(arg3)) {
                        sender.sendMessage(plugin.getMessages().getGroupDoesNotExists());
                        return false;
                    }
                    Group group = plugin.getGroups().get(arg3);
                    plugin.setGroup(group, arg2);
                    sender.sendMessage(plugin.getMessages().getMessage(plugin.getMessages().getGroupSet(), arg2, arg3));
                    break;
                default:
                    help(sender);
            }
        } else {
            help(sender);
        }

        return false;
    }

    private void help(CommandSender sender) {
        sender.sendMessage(plugin.getMessages().getMessage(plugin.getMessages().getUsage(), "/perms groups"));
        sender.sendMessage(plugin.getMessages().getMessage(plugin.getMessages().getUsage(), "/perms reload"));
        sender.sendMessage(plugin.getMessages().getMessage(plugin.getMessages().getUsage(), "/perms createGroup <Gruppe>"));
        sender.sendMessage(plugin.getMessages().getMessage(plugin.getMessages().getUsage(), "/perms deleteGroup <Gruppe>"));
        sender.sendMessage(plugin.getMessages().getMessage(plugin.getMessages().getUsage(), "/perms getGroup <Group>"));
        sender.sendMessage(plugin.getMessages().getMessage(plugin.getMessages().getUsage(), "/perms getPlayer <Spieler>"));
        sender.sendMessage(plugin.getMessages().getMessage(plugin.getMessages().getUsage(), "/perms add <Gruppe> <Permission>"));
        sender.sendMessage(plugin.getMessages().getMessage(plugin.getMessages().getUsage(), "/perms remove <Gruppe> <Permission>"));
        sender.sendMessage(plugin.getMessages().getMessage(plugin.getMessages().getUsage(), "/perms setGroup <Spieler> <Gruppe>"));
    }
}

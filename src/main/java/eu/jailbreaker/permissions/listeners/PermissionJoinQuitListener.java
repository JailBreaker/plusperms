package eu.jailbreaker.permissions.listeners;

import eu.jailbreaker.permissions.PermissionsHandler;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.permissions.PermissionAttachment;

/********************************************************************************
 *    Urheberrechtshinweis                                                      *
 *    Copyright © JailBreaker 2018                                           *
 *    Projekt: Permissions                                                  *
 *    Erstellt: 15.12.2018 / 16:29                                               *
 *                                                                              *
 *    Alle Inhalte dieses Quelltextes sind urheberrechtlich geschützt.          *
 *    Das Urheberrecht liegt, soweit nicht ausdrücklich anders gekennzeichnet,  *
 *    bei JailBreaker. Alle Rechte vorbehalten.                                 *
 *                                                                              *
 *    Jede Art der Vervielfältigung, Verbreitung, Vermietung, Verleihung,       *
 *    öffentlichen Zugänglichmachung oder andere Nutzung                        *
 *    bedarf der ausdrücklichen, schriftlichen Zustimmung von JailBreaker.      *
 *                                                                              *
 *    YouTube: https://www.youtube.com/JailBreaker                              *
 *    Twitter: https://twitter.com/JailBreakerTV                                *
 *    Webseite: https://www.jailbreaker.eu                                      *
 *    Teamspeak: ts.jailbreaker.eu                                              *
 *******************************************************************************/

public class PermissionJoinQuitListener implements Listener {

    private final PermissionsHandler plugin;

    public PermissionJoinQuitListener(final PermissionsHandler plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void handlePlayerJoin(final AsyncPlayerPreLoginEvent event) {
        plugin.createPlayer(event.getName());
    }

    @EventHandler
    public void handleJoin(final PlayerJoinEvent event) {

        PermissionAttachment attachment = event.getPlayer().addAttachment(plugin);

        for (String permissions : plugin.getPlayers().get(event.getPlayer().getName()).getGroup().getPermissions()) {
            attachment.setPermission(permissions, true);
        }

        event.getPlayer().sendMessage("§aPermissions loaded§8!");

    }

    @EventHandler
    public void handlePlayerQuit(final PlayerQuitEvent event) {
        final Player player = event.getPlayer();

        player.getEffectivePermissions().clear();
    }
}

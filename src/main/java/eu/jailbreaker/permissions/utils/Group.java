package eu.jailbreaker.permissions.utils;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/********************************************************************************
 *    Urheberrechtshinweis                                                      *
 *    Copyright © JailBreaker 2018                                           *
 *    Projekt: Permissions                                                  *
 *    Erstellt: 15.12.2018 / 16:33                                               *
 *                                                                              *
 *    Alle Inhalte dieses Quelltextes sind urheberrechtlich geschützt.          *
 *    Das Urheberrecht liegt, soweit nicht ausdrücklich anders gekennzeichnet,  *
 *    bei JailBreaker. Alle Rechte vorbehalten.                                 *
 *                                                                              *
 *    Jede Art der Vervielfältigung, Verbreitung, Vermietung, Verleihung,       *
 *    öffentlichen Zugänglichmachung oder andere Nutzung                        *
 *    bedarf der ausdrücklichen, schriftlichen Zustimmung von JailBreaker.      *
 *                                                                              *
 *    YouTube: https://www.youtube.com/JailBreaker                              *
 *    Twitter: https://twitter.com/JailBreakerTV                                *
 *    Webseite: https://www.jailbreaker.eu                                      *
 *    Teamspeak: ts.jailbreaker.eu                                              *
 *******************************************************************************/

@Getter
@Setter
public class Group {

    private final String name;
    private String prefix, suffix, display;
    private List<String> permissions;

    public Group(final String name) {
        this.name = name;
    }

    public Group(final String name, final List<String> permissions) {
        this.name = name;
        this.permissions = permissions;
    }

    public boolean hasPermission(final String permission) {
        return permissions.contains(permission);
    }

}

package eu.jailbreaker.permissions.utils;

import lombok.Getter;
import lombok.Setter;

/********************************************************************************
 *    Urheberrechtshinweis                                                      *
 *    Copyright © JailBreaker 2018                                           *
 *    Projekt: Permissions                                                  *
 *    Erstellt: 15.12.2018 / 16:34                                               *
 *                                                                              *
 *    Alle Inhalte dieses Quelltextes sind urheberrechtlich geschützt.          *
 *    Das Urheberrecht liegt, soweit nicht ausdrücklich anders gekennzeichnet,  *
 *    bei JailBreaker. Alle Rechte vorbehalten.                                 *
 *                                                                              *
 *    Jede Art der Vervielfältigung, Verbreitung, Vermietung, Verleihung,       *
 *    öffentlichen Zugänglichmachung oder andere Nutzung                        *
 *    bedarf der ausdrücklichen, schriftlichen Zustimmung von JailBreaker.      *
 *                                                                              *
 *    YouTube: https://www.youtube.com/JailBreaker                              *
 *    Twitter: https://twitter.com/JailBreakerTV                                *
 *    Webseite: https://www.jailbreaker.eu                                      *
 *    Teamspeak: ts.jailbreaker.eu                                              *
 *******************************************************************************/

@Getter
@Setter
public class PermissionPlayer {

    private final String name;
    private Group group;

    public PermissionPlayer(final String name, final Group group) {
        this.name = name;
        this.group = group;
    }
}

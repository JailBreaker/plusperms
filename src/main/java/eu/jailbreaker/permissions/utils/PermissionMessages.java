package eu.jailbreaker.permissions.utils;

import eu.jailbreaker.permissions.PermissionsHandler;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.ChatColor;

import java.text.MessageFormat;

/********************************************************************************
 *    Urheberrechtshinweis                                                      *
 *    Copyright © JailBreaker 2018                                           *
 *    Projekt: Permissions                                                  *
 *    Erstellt: 15.12.2018 / 19:07                                               *
 *                                                                              *
 *    Alle Inhalte dieses Quelltextes sind urheberrechtlich geschützt.          *
 *    Das Urheberrecht liegt, soweit nicht ausdrücklich anders gekennzeichnet,  *
 *    bei JailBreaker. Alle Rechte vorbehalten.                                 *
 *                                                                              *
 *    Jede Art der Vervielfältigung, Verbreitung, Vermietung, Verleihung,       *
 *    öffentlichen Zugänglichmachung oder andere Nutzung                        *
 *    bedarf der ausdrücklichen, schriftlichen Zustimmung von JailBreaker.      *
 *                                                                              *
 *    YouTube: https://www.youtube.com/JailBreaker                              *
 *    Twitter: https://twitter.com/JailBreakerTV                                *
 *    Webseite: https://www.jailbreaker.eu                                      *
 *    Teamspeak: ts.jailbreaker.eu                                              *
 *******************************************************************************/

@Getter
@Setter
public final class PermissionMessages {

    private final String prefix, groupExists, mainColor, groupListTitle, groupCreated, groupDoesNotExists, groupDeleted, groupInformation, playerDoesNotExists,
            playerInformation, groupAlreadyHasPermission, permissionAdded, groupDoesNotHavePermission, permissionRemoved, groupSet, noPerm, usage;

    public PermissionMessages(final PermissionsHandler plugin) {
        this.prefix = plugin.getConfig().getString("Messages.Prefix").replaceAll("&", "§").replaceAll("%n", "\n");
        this.groupExists = prefix + plugin.getConfig().getString("Messages.GroupExists").replaceAll("&", "§").replaceAll("%n", "\n");
        this.groupDoesNotExists = prefix + plugin.getConfig().getString("Messages.GroupDoesNotExists").replaceAll("&", "§").replaceAll("%n", "\n");
        this.mainColor = plugin.getConfig().getString("Messages.MainColor").replaceAll("&", "§").replaceAll("%n", "\n");
        this.groupListTitle = prefix + plugin.getConfig().getString("Messages.GroupListTitle").replaceAll("&", "§").replaceAll("%n", "\n");
        this.groupCreated = prefix + plugin.getConfig().getString("Messages.Command.GroupCreated").replaceAll("&", "§").replaceAll("%n", "\n");
        this.groupDeleted = prefix + plugin.getConfig().getString("Messages.Command.GroupDeleted").replaceAll("&", "§").replaceAll("%n", "\n");
        this.groupInformation = prefix + plugin.getConfig().getString("Messages.Command.GroupInformation").replaceAll("&", "§").replaceAll("%n", "\n");
        this.playerDoesNotExists = prefix + plugin.getConfig().getString("Messages.PlayerDoesNotExists").replaceAll("&", "§").replaceAll("%n", "\n");
        this.playerInformation = prefix + plugin.getConfig().getString("Messages.Command.PlayerInformation").replaceAll("&", "§").replaceAll("%n", "\n");
        this.groupAlreadyHasPermission = prefix + plugin.getConfig().getString("Messages.GroupAlreadyHasPermission").replaceAll("&", "§").replaceAll("%n", "\n");
        this.permissionAdded = prefix + plugin.getConfig().getString("Messages.Command.PermissionAdded").replaceAll("&", "§").replaceAll("%n", "\n");
        this.groupDoesNotHavePermission = prefix + plugin.getConfig().getString("Messages.GroupDoesNotHavePermission").replaceAll("&", "§").replaceAll("%n", "\n");
        this.permissionRemoved = prefix + plugin.getConfig().getString("Messages.Command.PermissionRemoved").replaceAll("&", "§").replaceAll("%n", "\n");
        this.groupSet = prefix + plugin.getConfig().getString("Messages.Command.GroupSet").replaceAll("&", "§").replaceAll("%n", "\n");
        this.noPerm = prefix + plugin.getConfig().getString("Messages.NoPerm").replaceAll("&", "§").replaceAll("%n", "\n");
        this.usage = prefix + plugin.getConfig().getString("Messages.Command.Usage").replaceAll("&", "§").replaceAll("%n", "\n");
    }

    public String getMessage(String path, Object... objects) {
        return ChatColor.translateAlternateColorCodes('&', MessageFormat.format(path, objects)).replaceAll("%n", "\n");
    }
}
